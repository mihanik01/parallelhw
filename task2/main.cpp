#include <omp.h>
#include <ctime>
#include <iostream>
#include <algorithm>
#include <random>
#include <cstdint>
#include <string>

template<typename T>
bool findElement(const T &element, const T array[], const size_t arrayLength,
  size_t &outIndex) {

  int Nthr = omp_get_max_threads();
  // int minFoundThreadNum = Nthr;
  const size_t totalForThreads = arrayLength / Nthr;
  // size_t foundIndices[Nthr];
  bool elementFound = false;
  #pragma omp parallel num_threads(Nthr)
  {
    const int threadNum = omp_get_thread_num();
    const size_t begin = threadNum * totalForThreads;
    size_t end = std::min(begin + totalForThreads, arrayLength);
    if (0 == end && 0 == threadNum) {
      end = arrayLength;
    }
    for (size_t i = begin; i < end; ++i) {
      if (array[i] == element) {
        #pragma omp atomic write
        elementFound = true;
        #pragma omp atomic write
        outIndex = i;
        /*
        foundIndices[threadNum] = i;
        #pragma omp critical(minimalFoundThreadNumber)
        if (minFoundThreadNum > threadNum) {
          minFoundThreadNum = threadNum;
        }
        */
      }

      bool elementFoundRead;
      #pragma omp atomic read
      elementFoundRead = elementFound;
      if (elementFoundRead) {
        break;
      }

      /*
      int minFoundTNum;
      #pragma omp atomic read
      minFoundTNum = minFoundThreadNum;
      // ???
      // if (minFoundTNum < Nthr)
      if (minFoundTNum <= threadNum) {
        break;
      }
      */
    }
  }

  return elementFound;
  /*
  if (minFoundThreadNum < Nthr) {
    outIndex = foundIndices[minFoundThreadNum];
    return true;
  } else {
    return false;
  }
  */
}

template<typename T>
bool findElementNoPar(const T &element, const T array[],
  const size_t arrayLength, size_t &outIndex) {
  for (size_t i = 0; i < arrayLength; ++i) {
    if (array[i] == element) {
      outIndex = i;
      return true;
    }
  }

  return false;

}

int main(int argc, char **argv) {
  std::random_device rd;
  std::mt19937 rng(rd());
  std::uniform_int_distribution<> dis(INT32_MIN, INT32_MAX);

  if (argc < 2) {
    std::cout << "Wrong usage" << std::endl;
    return 0;
  }

  size_t arraySize = std::stol(argv[1]);
  
  int32_t *array = new int32_t[arraySize];
  for (size_t i = 0; i < arraySize; i++) {
    array[i] = dis(rng);
  }
  std::uniform_int_distribution<> disIdx(0, arraySize - 1);
  size_t elementIdx =
    //arraySize - 1;
    disIdx(rng);
  int32_t element = array[elementIdx];

  int Nthr = omp_get_max_threads();
  // std::cout << "Nthr: " << Nthr << std::endl;
  // std::cout << "Searching for element " << element << " in position "
  //   << elementIdx << ", part index " << (elementIdx / (arraySize / Nthr))
  //   << std::endl;

  size_t outIndex;
  double begin = omp_get_wtime();
  bool found = findElement(element, array, arraySize, outIndex);
  double end = omp_get_wtime();

  size_t noParOutIndex;
  double noParBegin = omp_get_wtime();
  bool noParFound = findElementNoPar(element, array, arraySize, noParOutIndex);
  double noParEnd = omp_get_wtime();

  delete[] array;

  if (found) {
    // std::cout << "Element " << element << " is in position " << outIndex
    //  << std::endl;
  } else {
    // std::cout << "Element " << element << " was not found in an array"
    //  << std::endl;
  }
  double elapsedSeconds = double(end - begin);
  // std::cout << elapsedSeconds << " seconds elapsed" << std::endl;
  std::cout << elapsedSeconds;

  /*
  if (noParFound) {
    std::cout << "Element " << element << " was found by non-parallel algo in "
      << noParOutIndex << std::endl;
  } else {
    std::cout << "Element was not found by non-parallel algorithm" << std::endl;
  }
  */
  double noParElapsed = double(noParEnd - noParBegin);
  //std::cout << noParElapsed << " seconds elapsed for non-parallel" << std::endl;
  std::cout << "," << noParElapsed << "," << (noParElapsed / elapsedSeconds) << std::endl;

  return 0;
}
