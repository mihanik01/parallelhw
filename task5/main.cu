#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <chrono>
#include <random>
#include <string>
#include <cstdint>
#include <cstring>
#include <iostream>

template<typename T>
__global__ void mulOnGpu(
  const T *a, const T* b, T *c,
  size_t l, size_t m, size_t n
) {
  size_t i = threadIdx.x + blockIdx.x * blockDim.x;
  size_t j = threadIdx.y + blockIdx.y * blockDim.y;
  if (i < l && j < n) {
    T sum = 0;
    for (size_t k = 0; k < m; ++k) {
      sum += a[i * m + k] * b[k * n + j];
    }
    c[i * n + j] = sum;
  }
}

template<typename T>
void mulOnCpu(const T *a, const T *b, T *c,
  size_t l, size_t m, size_t n) {
  for (size_t i = 0; i < l; ++i) {
    for (size_t j = 0; j < n; ++j) {
      T sum = 0;
      for (size_t k = 0; k < m; ++k) {
        sum += a[i * m + k] * b[k * n + j];
      }
      c[i * n + j] = sum;
    }
  }
}


void fillMatrix(int32_t *matrix, size_t rows, size_t columns) {
  std::random_device rd;
  std::mt19937 rng(rd());
  std::uniform_int_distribution<> fillDistribution(-50, 50);//(INT32_MIN, INT32_MAX);
  for (size_t i = 0; i < rows * columns; ++i) {
    matrix[i] = fillDistribution(rd);
  }
}

template<typename T>
void outputMatrix(const T *matrix, size_t rows, size_t columns) {
  for (size_t i = 0; i < rows; ++i) {
    for (size_t j = 0; j < columns; ++j) {
      std::cout << matrix[i * columns + j] << " ";
    }
    std::cout << std::endl;
  }
}

int main(int argc, char **argv) {
  std::random_device rd;
  std::mt19937 rng(rd());
  std::uniform_int_distribution<> dimenDistribution(500, 1000);

  if (argc < 4) {
    std::cout << "Wrong usage" << std::endl;
    return 0;
  }

  size_t l, m, n;
  /*
  l = dimenDistribution(rd);
  m = dimenDistribution(rd);
  n = dimenDistribution(rd);
  */
  l = std::stol(argv[1]);
  m = std::stol(argv[2]);
  n = std::stol(argv[3]);

  // std::cout << "Dimenstions" << std::endl
  //  << "l: " << l << " m: " << m << " n: " << n << std::endl;

  int32_t *a, *b, *c;
  a = new int32_t[l * m];
  b = new int32_t[m * n];
  c = new int32_t[l * n];

  size_t aSize, bSize, cSize;
  aSize = l * m * sizeof(a[0]);
  bSize = m * n * sizeof(b[0]);
  cSize = l * n * sizeof(c[0]);

  fillMatrix(a, l, m);
  fillMatrix(b, m, n);
  memset(c, 0, cSize);


  // *******************************
  //           CPU Part
  // *******************************

  auto cpuBegin = std::chrono::steady_clock::now();
  mulOnCpu(a, b, c, l, m, n);
  auto cpuEnd = std::chrono::steady_clock::now();

  auto cpuMs = std::chrono::duration_cast<std::chrono::milliseconds>(
    cpuEnd - cpuBegin
  ).count();
  // std::cout << "CPU time (ms): " << cpuMs << std::endl;
  std::cout << cpuMs;

  // std::cout << std::endl << "CPU result: " << std::endl;
  // outputMatrix(c, 1, 10);//(c, l, n);
  // std::cout << std::endl;

  memset(c, 0, cSize);

  // *******************************
  //           GPU Part
  // *******************************

  int32_t *cudaA, *cudaB, *cudaC;

  if (cudaSuccess != cudaMalloc(&cudaA, aSize)) {
    // TODO: free
  }

  if (cudaSuccess != cudaMalloc(&cudaB, bSize)) {
    // TODO: free
  }

  if (cudaSuccess != cudaMalloc(&cudaC, cSize)) {
    // TODO: free
  }

  cudaMemcpy(cudaA, a, aSize, cudaMemcpyHostToDevice);
  cudaMemcpy(cudaB, b, bSize, cudaMemcpyHostToDevice);

  // TODO: no magic (?)
  auto nThreads = 32;
  dim3 threads(nThreads, nThreads);

  auto nBlocksX = l / threads.x;
  nBlocksX     += (0 == l % threads.x) ? 0 : 1;
  auto nBlocksY = n / threads.y;
  nBlocksY     += (0 == n % threads.y) ? 0 : 1;

  dim3 blocks(nBlocksX, nBlocksY);

  auto gpuBegin = std::chrono::steady_clock::now();
  mulOnGpu <<< blocks, threads >>> (cudaA, cudaB, cudaC, l, m, n);
  cudaDeviceSynchronize();
  auto gpuEnd = std::chrono::steady_clock::now();
  
  auto gpuMs = std::chrono::duration_cast<std::chrono::milliseconds>(
    gpuEnd - gpuBegin
  ).count();
    // std::cout << "GPU time (ms): " << gpuMs << std::endl;
  std::cout << "," << gpuMs << "," << ((double)cpuMs / (double)gpuMs) << std::endl;

  // TODO: check errors (?)
  cudaMemcpy(c, cudaC, cSize, cudaMemcpyDeviceToHost);

//  std::cout << std::endl << "GPU result: " << std::endl;
//  outputMatrix(c, 1, 10);//(c, l, n);
//  std::cout << std::endl;

  cudaFree(cudaA);
  cudaFree(cudaB);
  cudaFree(cudaC);

  delete[] a;
  delete[] b;
  delete[] c;

  return 0;
}
