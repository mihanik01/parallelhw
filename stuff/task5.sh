#!/bin/bash
#PBS -k o
#PBS -j oe
#$PBS_O_WORKDIR=/home/student21m09/hw
PBS_O_WORKDIR=/home/student21m09/hw

for l in 100 500 1000
do
	for m in 200 300
	do
		echo "Running for $l $m 500"
		for i in {1..10}
		do
			$PBS_O_WORKDIR/task5 $l $m 500
		done
	done
done

