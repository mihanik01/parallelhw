#!/bin/bash
#PBS -k o
#PBS -j oe
#$PBS_O_WORKDIR=/home/student21m09/hw
PBS_O_WORKDIR=/home/student21m09/hw

for n in 1000000 10000000 250000000 500000000 1000000000
do
	echo "Size: $n"
	for i in {1..10}
	do
	  $PBS_O_WORKDIR/task2 $n
	done
done

